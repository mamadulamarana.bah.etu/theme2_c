#include <stdio.h>
#include "put_numbers.h"

static int put_digit(int digit) {
    if (digit >= 0 &&  digit < 10) {
        putchar(digit+'0');
        return 0;
    }
    return -1;
}

static int put_hdigit(int h) {
    if (h >= 10 && h < 16) {
        putchar(h + 'A' - 10);
        return 0;
    }
    return put_digit(h);
}

static int putdecP(int d) {
    if ( d >= 10) {
        int r = d%10;
        int q = d/10;
        putdecP(q);
        put_digit(r);
        return 0;
    }
    else {
        return put_digit(d);
    }
}

int putdec(int d) {
    if( d == -2147483648) {
        int r = d%10;
        int q = d/10;
        r = -r;
        putdec(q);
        return put_digit(r);
    }

    else if ( d < 0) {
        d = -d;
        putchar('-');
        return putdecP(d);
    }
    else {
        return putdecP(d);
    }
}

int putbin(unsigned int b) {
    if ( (b >> 1)) {
        int c = putbin(b >> 1);
        if (c) {
            return c;
        }
    }
    return put_digit(b & 1);
}

int puthex(int h){
 
    unsigned int m = (unsigned int )h;
    if (m >> 4){
        int c = puthex(m >> 4);
        if (c) {
            return c;
        }
    }
    return put_hdigit(h & 0b1111);
}
