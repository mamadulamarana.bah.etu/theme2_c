#ifndef _ABS_H
    #define _ABS_H
    static inline int abs(int x) { return (x<0)?-x:x; }
#endif

